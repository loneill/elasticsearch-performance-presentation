package loneill;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.codecs.simpletext.SimpleTextCodec;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.apache.lucene.index.IndexWriterConfig.OpenMode;

public class Main {

    private static final Collection<Listing> listings = Collections.unmodifiableList(Arrays.asList(
            new Listing(1, "iPhone 6S for sale"),
            new Listing(2, "Blue tent for sale")));

    public static void main(String[] args) throws IOException {
        String indexPath = args[0];

        Directory dir = FSDirectory.open(Paths.get(indexPath));
        Analyzer analyzer = new StandardAnalyzer();

        IndexWriterConfig iwc = new IndexWriterConfig(analyzer);

        // or you can use the default codec: iwc.setCodec(new Lucene70Codec());
        iwc.setCodec(new SimpleTextCodec());

        // Lucene will usually wrap together small segments into a single .cfs file to reduce file descriptors
        //    this setting will disable this
        iwc.setUseCompoundFile(false);

        // Create a new index in the directory, removing any previously indexed documents:
        iwc.setOpenMode(OpenMode.CREATE);

        // Write documents into the index's buffer
        IndexWriter writer = new IndexWriter(dir, iwc);
        for (Listing listing : listings) {
            Document doc = ToDocument(listing);
            writer.addDocument(doc);
        }

        // Flush the documents to disk
        writer.flush();

        // Delete a document & flush to disk, but don't trigger a merge
        writer.deleteDocuments(LongPoint.newExactQuery("id", 1));
        writer.flush();
    }

    private static Document ToDocument(Listing listing) {
        Document doc = new Document();
        doc.add(new LongPoint("id", listing.id));
        doc.add(new TextField("title", listing.title, Field.Store.YES));
        return doc;
    }

    public static class Listing
    {
        final long id;
        final String title;

        Listing(long id, String title) {
            this.id = id;
            this.title = title;
        }
    }
}
